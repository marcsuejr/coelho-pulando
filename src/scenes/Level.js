// Level.js
import { Math, Scene } from "phaser";
import Carrot from "../objects/carrot";

export default class Level extends Scene {
    /** @type {Phaser.Physics.Arcade.Sprite} */
    player;

    /** @type {Phaser.Physics.Arcade.StaticGroup} */
    platforms;

    /**@type {Phaser.Types.Input.Keyboard.CursorKeys} */
    cursors;

    /** @type {Phaser.Physics.Arcade.StaticGroup} */
    carrots;

    points=0;

    /**@type {Phaser.GameObjects.Text} */
    pointsText;

    constructor() {
        super('level');
    }

    preload() {
        this.load.image('background', 'assets/bg_layer1.png');
        this.load.image('platform', 'assets/ground_grass.png');
        this.load.image('bunny-stand', 'assets/bunny1_stand.png');
        this.load.image('carrot', 'assets/carrot.png')
        this.load.image('bunny-jump', 'assets/bunny1_jump.png');

        this.load.audio('jump', 'assets/sfx/jump.ogg')
        this.load.audio('game-over', 'assets/sfx/gameover.ogg')
        this.load.audio('eat','assets/sfx/eat.ogg')
    }

    create() {
        // Background
        this.add.image(240, 320, 'background')
            .setScrollFactor(0, 0);

        // Plataforma
        //const platform = this.physics.add.staticImage(240, 320, 'platform')
        //    .setScale(0.5);

        // Grupo de plataformas
        this.platforms = this.physics.add.staticGroup();

        for (let i = 0; i < 5; i++) {
            const x = Math.Between(80, 400);
            const y = 150 * i;

            const platform = this.platforms.create(x, y, 'platform');
            platform.setScale(0.5);
            platform.body.updateFromGameObject();
        }


        // Criando o Player
        this.player = this.physics.add.image(240, 120, 'bunny-stand')
            .setScale(0.5);
        
        // Faz os elementos colidirem
        this.physics.add.collider(this.player, this.platforms);

        // Disabilitar a colisão do coelho nas laterais e em cima
        this.player.body.checkCollision.up = false;
        this.player.body.checkCollision.left = false;
        this.player.body.checkCollision.right = false;

        // Câmera
        this.cameras.main.startFollow(this.player);

        //DEAD ZONE PAR CAMERA
        this.cameras.main.setDeadzone(this.scale.width *1.5)
        

        //cursores
        this.cursors = this.input.keyboard.createCursorKeys();

        //cenouras
        this.carrots = this.physics.add.group({
            classType: Carrot
        });
        //quando se colidem
        this.physics.add.collider(this.carrots, this.platforms);
        
        //se encontram
        this.physics.add.overlap(this.player, this.carrots, this.handleColletCarrot, undefined, this)

        //texto da pontuação (mesmo style do css, porem camelCase)
        const style = {color: '8000', fontSize: 24};
        this.pointsText= this.add.text(240,10, 'Cenouras: '+this.points,style);
        this.pointsText.setScrollFactor(0) //os parametros sao x,y mas se vc passar só um ele entende que é para os dois
        this.pointsText.setOrigin(0.5,0) //ponto pivo (ancora) da origem a escala vai entre 0 à 1 e os intervalos são decimais, extrema esquerda é 0 extrema direita 1

        // let rect = this.add.rectangle(240,300,100,50, 0xffcc00)

    }

    //TODA INTERAÇÃO TEM QUE SER FEITA NO UPDATE
    update(time, delta) {
        // Pulando
        const touchingGround = this.player.body.touching.down;
        
        if ( touchingGround ) {
            this.player.setVelocityY(-300);
            this.sound.play('jump')

            //MUDAR A IMGAEM DO COELHO QUANDO ELE PULAR
            this.player.setTexture('bunny-jump');
        }
        
        //VERIFICAR A VELOCIDADE DO COELHOR PARA VOLTAR A IMAGEM DO COELHO NORMAL
        let velocityPlayerY = this.player.body.velocity.y;
        
        if (velocityPlayerY > -190 && this.player.texture.key != 'bunny-stand')
            this.player.setTexture('bunny-stand');


        // Reusando as plataformas
        this.platforms.children.iterate(child => {
            /** @type {Phaser.Physics.Arcade.Sprite} */
            const platform = child;

            // Pegar a posição Y da Câmera
            const scrollY = this.cameras.main.scrollY;
            if ( platform.y >= scrollY + 700 ) 
            {
                platform.x = Math.Between(80, 400);
                platform.y = scrollY - Math.Between(0, 10);
                platform.body.updateFromGameObject();

                //criar uma cenoura acima
                this.addCarrotAbove(platform)
                
            }
        })

        //cursores direita e esquerda
        if (this.cursors.left.isDown){
            this.player.setVelocityX(-200);
        } 
        else if (this.cursors.right.isDown)
        {
            this.player.setVelocityX(200);
        }
        //SE PARAR DE APERTAR RESETA A VELICUDADE
        else
        {
            this.player.setVelocityX(0);
        }

        //testando se o coelho caiu
        let bottomPlatform = this.findBottomPlatform();
        if(this.player.y > bottomPlatform.y + 800){
            console.log("GAME OVER")
            this.sound.play('game-over')
            this.scene.start('game-over')
        }

    }


    addCarrotAbove(platform) {
        const y = platform.y - platform.displayHeight;

        const carrot = this.carrots.get(platform.x, y ,'carrot');

        carrot.setActive(true);
        carrot.setVisible(true)

        this.add.existing(carrot)

        carrot.body.setSize(carrot.width, carrot.height);
        this.physics.world.enable(carrot);
    }

    handleColletCarrot(player, carrot)
    {
        this.carrots.killAndHide(carrot);
        this.physics.world.disableBody(carrot.body);

        this.points++;
        this.pointsText.text = 'Cenouras: '+this.points

        this.sound.play('eat');
    }

    //procura a plataforma mais baixa
    findBottomPlatform(){
        let plataforms = this.platforms.getChildren();
        let bottomPlatform = plataforms[0];

        for (let i = 1; i < this.platforms.getLength; i++)
        {
            let platform = platforms[i];

            if (platform.y < bottomPlatform.y)
                continue;

            bottomPlatform = platform
        }
        return bottomPlatform
    }
}